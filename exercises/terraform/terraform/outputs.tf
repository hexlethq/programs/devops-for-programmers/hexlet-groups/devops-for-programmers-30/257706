output "ip-address-01" {
  value       = digitalocean_droplet.app.*.ipv4_address
  description = "The public IP addresses of droplets"
}

output "ip-address-lb" {
  value       = digitalocean_loadbalancer.public.ip
  description = "The public IP address of Load Balancer"
}

