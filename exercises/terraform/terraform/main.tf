terraform {
  required_version = ">= 1.0.0"

  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}

provider "digitalocean" {}

resource "digitalocean_droplet" "app" {
  count     = 2
  image     = "docker-20-04"
  name      = "web-terraform-homework-0${count.index + 1}"
  region    = "ams3"
  size      = "s-1vcpu-1gb"
  user_data = file("web_app.yml")
}

resource "digitalocean_loadbalancer" "public" {
  name   = "loadbalancer-1"
  region = "ams3"

  forwarding_rule {
    entry_port     = 80
    entry_protocol = "http"

    target_port     = 5000
    target_protocol = "http"
  }

  healthcheck {
    port     = 5000
    protocol = "tcp"
  }

  droplet_ids = digitalocean_droplet.app.*.id
}
